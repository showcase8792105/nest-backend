import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { LogoutAuthDtoDto } from '../src/auth/dto/logout-auth.dto';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  /*it('/ (POST)', () => {
    const obj: LogoutAuthDtoDto = {
      userId: '6368dec186f28152aef24803',
    };
    return request(app.getHttpServer())
      .post('/')
      .send(obj)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect('Hello World!');
  });*/
  it('auth/logout', function (done) {
    const obj: LogoutAuthDtoDto = {
      userId: '6368dec186f28152aef24803',
    };
    request(app)
      .post('/auth/logout')
      .send(obj)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function (res) {
        console.log('res----' + JSON.stringify(res.body));
        // res.body.id = 'some fixed id';
        // res.body.name = res.body.name.toLowerCase();
      })
      .expect(
        200,
        {
          id: 'some fixed id',
          name: 'john',
        },
        done,
      );
  });

  afterAll(async () => {
    await app.close();
  });
});
