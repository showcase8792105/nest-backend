import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role, RoleDocument } from './schemas/roles.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class RolesService {
  constructor(@InjectModel(Role.name) private roleModel: Model<RoleDocument>) {}
  async create(createRoleDto: CreateRoleDto): Promise<Role> {
    const createdCat = new this.roleModel(createRoleDto);
    return createdCat.save();
  }

  async findAll(): Promise<Role[]> {
    return this.roleModel.find().lean().exec();
  }

  async findOne(id: string): Promise<Role | null> {
    return this.roleModel.findOne({ _id: id }).lean().exec();
  }

  async find(query: Record<string, any>): Promise<Role[]> {
    return this.roleModel.find(query).lean().exec();
  }

  async remove(id: string): Promise<Role | null> {
    return await this.roleModel.findByIdAndRemove({ _id: id }).exec();
  }
}
