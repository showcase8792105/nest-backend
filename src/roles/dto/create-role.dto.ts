export class CreateRoleDto {
  route: string; // route
  permission: string; // permission type
  roles: [string]; // roles allowed
}
