import { Test, TestingModule } from '@nestjs/testing';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role, RoleDocument } from './schemas/roles.schema';
import { CreateRoleDto } from './dto/create-role.dto';
import { Permission } from '../permissions/enums/permission.enum';

describe('RolesController', () => {
  let controller: RolesController;
  let service: RolesService;
  let mockRoleModel: Model<RoleDocument>;
  const role = new Role();
  const roleId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RolesController],
      providers: [
        RolesService,
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
      ],
    }).compile();

    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    service = module.get<RolesService>(RolesService);
    controller = module.get<RolesController>(RolesController);
  });

  it('(RolesController) should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('(RolesService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(RolesModel) should be defined', () => {
    expect(mockRoleModel).toBeDefined();
  });

  it('should create role', async () => {
    const obj: CreateRoleDto = {
      route: '/user',
      permission: Permission.READ,
      roles: ['admin'],
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(role as RoleDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(role);
  });

  it('should find all roles', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([role] as RoleDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([role]);
  });

  it('should find one roles', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(role as RoleDocument);
    await controller.findOne(roleId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(roleId)).toStrictEqual(role);
  });

  it('should remove role', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(role as RoleDocument);
    await controller.remove(roleId);
    expect(spy).toBeCalled();
    expect(await controller.remove(roleId)).toStrictEqual(role);
  });
});
