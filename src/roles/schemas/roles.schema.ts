import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type RoleDocument = Role & Document;

@Schema()
export class Role {
  @Prop()
  route: string;

  @Prop({})
  permission: string;

  @Prop({ type: [String], default: [] })
  roles: string[];
}

export const RoleSchema = SchemaFactory.createForClass(Role);
