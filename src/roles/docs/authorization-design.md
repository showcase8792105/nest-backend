Authorization Design
=======
* Author: Stewart Sabuka
* Date: 07 Nov 2022
* version: [v1.0.0]

Problem
---------------
Every route needs to authorize user before handling/process request, different user might have different permissions.

Solution
---------------
### What
* Every user needs to be assigned a role, Each role should be attached to different permissions depending on the route.
### How
* Create roles such as `player, liveOps, community, admim`, attach permissions to roles depending on the route
* Store the roles in to the database
* Attach route permissions via guards
* When user sends a request, the request is authenticated which decode token to get user basic data
  then query roles collection for the permissions attached to the current route
  
  
### Schemas
Role collection will be used to store role data

```typescript
const Role = new Schema({
  route: String, // /users, /role
  permission: String, // READ, WRITE, REMOVE
  roles: [String], // admin, player, community
})

export enum PermissionEnum {
  WRITE = 'WRITE',
  READ = 'READ',
  REMOVE = 'REMOVE'
}
```

### Request
* Client will add the issued token to the header and use it in every request
* Server will use authorization guard to check/validate user request

```typescript
@Controller('roles')
export class RolesController {
  constructor(private readonly roleService: RolesService) {}

@UseGuards(JwtAuthGuard, PermissionGuard)
@Permissions(Permission.WRITE)
@Post()
create(@Body() createRoleDto: CreateRoleDto) {
  return this.roleService.create(createRoleDto);
}

@UseGuards(JwtAuthGuard, PermissionGuard)
@Permissions(Permission.READ)
@Get()
findAll() {
  return this.roleService.findAll();
}

@UseGuards(JwtAuthGuard, PermissionGuard)
@Permissions(Permission.READ)
@Get(':id')
findOne(@Param('id') id: string) {
  return this.roleService.findOne(id);
}

@UseGuards(JwtAuthGuard, PermissionGuard)
@Permissions(Permission.REMOVE)
@Delete(':id')
remove(@Param('id') id: string) {
  return this.roleService.remove(+id);
}
}
```

### comments
* In the future, we might consider caching roles and permissions with redis bd instead of fetching from mongo on every request

Template references
===============

### ref
[web site]

[web site]: https://docs.nestjs.com/security/authentication
