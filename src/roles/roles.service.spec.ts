import { Test, TestingModule } from '@nestjs/testing';
import { RolesService } from './roles.service';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role, RoleDocument } from './schemas/roles.schema';

describe('RolesService', () => {
  let mockRoleModel: Model<RoleDocument>;
  let service: RolesService;
  const role = new Role();
  const roleId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolesService,
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    service = module.get<RolesService>(RolesService);
  });

  it('(RolesService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(RoleModel) should be defined', () => {
    expect(mockRoleModel).toBeDefined();
  });

  it('should get one role', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(role as RoleDocument);
    await service.findOne(roleId);
    expect(spy).toBeCalled();
    expect(await service.findOne(roleId)).toBe(role);
  });

  it('should find all roles', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([role] as RoleDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([role]);
  });

  it('should find roles', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([role] as RoleDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([role]);
  });

  it('should remove role', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(role as RoleDocument);
    await service.remove(roleId);
    expect(spy).toBeCalled();
    expect(await service.remove(roleId)).toBe(role);
  });

  it('should call role model', async () => {
    const spy = jest
      .spyOn(mockRoleModel, 'findOne')
      .mockResolvedValue(role as RoleDocument);
    await mockRoleModel.findOne({ _id: roleId });
    expect(spy).toBeCalled();
    expect(await mockRoleModel.findOne({ _id: roleId })).toBe(role);
  });
});
