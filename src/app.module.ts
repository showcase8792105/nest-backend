import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { RolesModule } from './roles/roles.module';
import { ConfigurationsModule } from './configurations/configurations.module';
import { WalletsModule } from './wallets/wallets.module';
import { BlackListModule } from './black-list/black-list.module';
import { TransactionsModule } from './transactions/transactions.module';
import { InventoryModule } from './inventory/inventory.module';
import { EnergyModule } from './energy/energy.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nest'),
    AuthModule,
    UsersModule,
    RolesModule,
    ConfigurationsModule,
    WalletsModule,
    BlackListModule,
    TransactionsModule,
    InventoryModule,
    EnergyModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
