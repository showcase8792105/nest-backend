import { Test, TestingModule } from '@nestjs/testing';
import { BlackListService } from './black-list.service';
import { Model } from 'mongoose';
import { BlackList, BlackListDocument } from './schemas/black-list.schema';
import { getModelToken } from '@nestjs/mongoose';
import { UsersService } from '../users/users.service';
import { User, UserDocument } from '../users/schemas/users.schema';

describe('BlackListService', () => {
  let service: BlackListService;
  let mockBlackListModel: Model<BlackListDocument>;
  let userService: UsersService;
  let mockUserModel: Model<UserDocument>;
  const blackList = new BlackList();
  const blackListId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BlackListService,
        UsersService,
        {
          provide: getModelToken(BlackList.name),
          useValue: Model,
        },
        {
          provide: getModelToken(User.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockUserModel = module.get<Model<UserDocument>>(getModelToken(User.name));
    userService = module.get<UsersService>(UsersService);
    mockBlackListModel = module.get<Model<BlackListDocument>>(
      getModelToken(BlackList.name),
    );
    service = module.get<BlackListService>(BlackListService);
  });

  it('(BlackListService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(BlackListModel) should be defined', () => {
    expect(mockBlackListModel).toBeDefined();
  });

  it('(BlackList) should be defined', () => {
    expect(blackList).toBeDefined();
  });

  it('(BlackListService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(UsersService) should be defined', () => {
    expect(userService).toBeDefined();
  });

  it('(UserModel) should be defined', () => {
    expect(mockUserModel).toBeDefined();
  });

  it('should get one blacklist', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(blackList as BlackListDocument);
    await service.findOne(blackListId);
    expect(spy).toBeCalled();
    expect(await service.findOne(blackListId)).toBe(blackList);
  });

  it('should find all blacklist', async () => {
    const query = { playerId: blackListId };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([blackList] as BlackListDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([blackList]);
  });

  it('should find blacklist', async () => {
    const query = { playerId: blackListId };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([blackList] as BlackListDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([blackList]);
  });

  it('should remove blacklist', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(blackList as BlackListDocument);
    await service.remove(blackListId);
    expect(spy).toBeCalled();
    expect(await service.remove(blackListId)).toBe(blackList);
  });
});
