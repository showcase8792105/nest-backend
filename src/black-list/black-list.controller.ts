import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { BlackListService } from './black-list.service';
import { CreateBlackListDto } from './dto/create-black-list.dto';
import { UpdateBlackListDto } from './dto/update-black-list.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { PermissionGuard } from '../permissions/guards/permission.guard';
import { Permissions } from '../permissions/decorators/permission.decorator';
import { Permission } from '../permissions/enums/permission.enum';

@Controller('black-list')
export class BlackListController {
  constructor(private readonly blackListService: BlackListService) {}

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.WRITE)
  @Post()
  create(@Body() createBlackListDto: CreateBlackListDto) {
    return this.blackListService.create(createBlackListDto);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get()
  findAll() {
    return this.blackListService.findAll();
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.blackListService.findOne(id);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.WRITE)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBlackListDto: UpdateBlackListDto,
  ) {
    return this.blackListService.update(id, updateBlackListDto);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.REMOVE)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.blackListService.remove(id);
  }
}
