import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import * as moment from 'moment';
export type BlackListDocument = BlackList & Document;

@Schema()
export class BlackList {
  @Prop({ type: Types.ObjectId, required: true })
  userId: string;

  @Prop({ type: String, required: false })
  email: string;

  @Prop({ type: String, required: false })
  uid: string;

  @Prop({ type: String, default: 'you have been banned temporarily' })
  message: string;

  @Prop({ type: Date, default: moment().add(7, 'days') })
  endDate: string;
}

export const BlackListSchema = SchemaFactory.createForClass(BlackList);
