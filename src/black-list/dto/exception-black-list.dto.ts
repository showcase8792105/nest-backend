export class ExceptionBlackListDto {
  message: string;
  endDate: string;
}
