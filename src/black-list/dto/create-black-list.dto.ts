import { IsNotEmpty } from 'class-validator';

export class CreateBlackListDto {
  @IsNotEmpty()
  userId: string;
  email?: string;
  uid?: string;
  message?: string;
  endDate?: Date;
}
