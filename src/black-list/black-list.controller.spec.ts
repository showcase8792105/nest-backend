import { Test, TestingModule } from '@nestjs/testing';
import { BlackListController } from './black-list.controller';
import { BlackListService } from './black-list.service';
import { Model } from 'mongoose';
import { BlackList, BlackListDocument } from './schemas/black-list.schema';
import { RolesService } from '../roles/roles.service';
import { Role, RoleDocument } from '../roles/schemas/roles.schema';
import { getModelToken } from '@nestjs/mongoose';
import { CreateBlackListDto } from './dto/create-black-list.dto';
import { UpdateBlackListDto } from './dto/update-black-list.dto';
import { UsersService } from '../users/users.service';
import { User, UserDocument } from '../users/schemas/users.schema';

describe('BlackListController', () => {
  let controller: BlackListController;
  let blackListService: BlackListService;
  let mockBlackListModel: Model<BlackListDocument>;
  let roleService: RolesService;
  let mockRoleModel: Model<RoleDocument>;
  let userService: UsersService;
  let mockUserModel: Model<UserDocument>;
  const blackList = new BlackList();
  const blacklistId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BlackListController],
      providers: [
        BlackListService,
        RolesService,
        UsersService,
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
        {
          provide: getModelToken(BlackList.name),
          useValue: Model,
        },
        {
          provide: getModelToken(User.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockUserModel = module.get<Model<UserDocument>>(getModelToken(User.name));
    userService = module.get<UsersService>(UsersService);
    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    roleService = module.get<RolesService>(RolesService);

    mockBlackListModel = module.get<Model<BlackListDocument>>(
      getModelToken(BlackList.name),
    );
    blackListService = module.get<BlackListService>(BlackListService);
    controller = module.get<BlackListController>(BlackListController);
  });

  it('(BlackListController) should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('(BlackListService) should be defined', () => {
    expect(blackListService).toBeDefined();
  });

  it('(BlackListModel) should be defined', () => {
    expect(mockBlackListModel).toBeDefined();
  });

  it('(BlackList) should be defined', () => {
    expect(blackList).toBeDefined();
  });

  it('(UsersService) should be defined', () => {
    expect(userService).toBeDefined();
  });

  it('(UserModel) should be defined', () => {
    expect(mockUserModel).toBeDefined();
  });

  it('should create blacklist', async () => {
    const obj: CreateBlackListDto = {
      userId: '636a3d70dfbd9b469c1d7981',
      message: 'illegal auth',
      endDate: new Date(),
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(blackList as BlackListDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(blackList);
  });

  it('should find all blacklist', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([blackList] as BlackListDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([blackList]);
  });

  it('should find one blacklist', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(blackList as BlackListDocument);
    await controller.findOne(blacklistId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(blacklistId)).toStrictEqual(blackList);
  });

  it('should update blacklist', async () => {
    const obj: UpdateBlackListDto = {
      userId: '636a3d70dfbd9b469c1d7981',
      message: 'illegal auth',
      endDate: new Date(),
    };
    const spy = jest
      .spyOn(controller, 'update')
      .mockResolvedValue(blackList as BlackListDocument);
    await controller.update(blacklistId, obj);
    expect(spy).toBeCalled();
    expect(await controller.update(blacklistId, obj)).toStrictEqual(blackList);
  });

  it('should remove blacklist', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(blackList as BlackListDocument);
    await controller.remove(blacklistId);
    expect(spy).toBeCalled();
    expect(await controller.remove(blacklistId)).toStrictEqual(blackList);
  });
});
