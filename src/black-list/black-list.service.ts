import { Injectable, Logger } from '@nestjs/common';
import { CreateBlackListDto } from './dto/create-black-list.dto';
import { UpdateBlackListDto } from './dto/update-black-list.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BlackList, BlackListDocument } from './schemas/black-list.schema';
import { UsersService } from '../users/users.service';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { User } from '../users/schemas/users.schema';

@Injectable()
export class BlackListService {
  private readonly logger = new Logger(BlackListService.name);
  constructor(
    @InjectModel(BlackList.name)
    private blackListModel: Model<BlackListDocument>,
    private usersService: UsersService,
  ) {}
  async create(createBlackListDto: CreateBlackListDto): Promise<BlackList> {
    const updateUser: UpdateUserDto = {
      refreshToken: '',
    };
    const user: User | null = await this.usersService.update(
      createBlackListDto.userId,
      updateUser,
    ); // remove refresh token from user
    if (user) {
      createBlackListDto.uid = user.uid;
      createBlackListDto.email = user.email;
    }
    const createdCat = new this.blackListModel(createBlackListDto);
    return createdCat.save();
  }

  async findAll(): Promise<BlackList[]> {
    return this.blackListModel.find().lean().exec();
  }

  async find(query: any): Promise<BlackList[]> {
    return this.blackListModel.find(query).lean().exec();
  }

  async findOne(id: string): Promise<BlackList | null> {
    return this.blackListModel.findOne({ _id: id }).lean().exec();
  }

  async update(
    id: string,
    updateBlackListDto: UpdateBlackListDto,
  ): Promise<BlackList | null> {
    return this.blackListModel.findOneAndUpdate(
      { _id: id },
      updateBlackListDto,
      { new: true },
    );
  }

  async remove(id: string): Promise<BlackList | null> {
    return this.blackListModel.findByIdAndRemove({ _id: id }).exec();
  }
}
