import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Logger,
  BadRequestException,
} from '@nestjs/common';
import { BlackListService } from '../black-list.service';
import { BlackList } from '../schemas/black-list.schema';
import { BlackListedException } from '../../common/exceptions';
import { ExceptionBlackListDto } from '../dto/exception-black-list.dto';
import * as moment from 'moment';

@Injectable()
export class BlackListGuard implements CanActivate {
  private readonly logger = new Logger(BlackListGuard.name);
  constructor(private blackListService: BlackListService) {}

  /**
   * canActivate
   * @param context
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    let items: BlackList[] = [];
    if (request.body.userId) {
      items = await this.blackListService.find({
        userId: request.body.userId,
      });
    } else if (request.body.email) {
      items = await this.blackListService.find({
        email: request.body.email,
      });
    } else if (request.body.uid) {
      items = await this.blackListService.find({
        uid: request.body.uid,
      });
    } else {
      throw new BadRequestException('no valid id to check for blacklist');
    }

    if (items.length > 0) {
      const exp: any = this.isExpired(items[0].endDate);
      if (exp.isExpired) {
        // @ts-ignore
        await this.blackListService.remove(items[0]._id);
        return true;
      }
      const obj: ExceptionBlackListDto = {
        message: items[0].message,
        endDate: items[0].endDate,
      };
      throw new BlackListedException(obj);
    }
    return true;
  }

  isExpired(endDate: string) {
    const current = moment();
    const status = moment(endDate).isBefore(current);
    const difference = moment(current).diff(endDate);

    const hours = moment.duration(difference).asHours();
    const minutes = moment.duration(difference).asMinutes();
    const seconds = moment.duration(difference).asSeconds();

    if (hours > 1 || hours < -1) {
      return { isExpired: status, duration: hours, unit: 'hours' };
    } else if (minutes > 1 || minutes < -1) {
      return { isExpired: status, duration: minutes, unit: 'minutes' };
    } else {
      return { isExpired: status, duration: seconds, unit: 'seconds' };
    }
  }
}
