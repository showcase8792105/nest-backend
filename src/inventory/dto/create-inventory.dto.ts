import { IsNotEmpty } from 'class-validator';

export class CreateInventoryDto {
  @IsNotEmpty()
  playerId: string;
  mainCategory: string;
  subCategory: string;
  level: number;
  type: string;
  data: any;
}
