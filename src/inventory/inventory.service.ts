import { Injectable, Logger } from '@nestjs/common';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UpdateInventoryDto } from './dto/update-inventory.dto';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Inventory, InventoryDocument } from './schemas/inventory.schema';
import * as mongoose from 'mongoose';
import { TransactionsService } from '../transactions/transactions.service';
import { CreateTransactionDto } from '../transactions/dto/create-transaction.dto';

@Injectable()
export class InventoryService {
  private readonly logger = new Logger(InventoryService.name);
  constructor(
    @InjectModel(Inventory.name)
    private inventoryModel: Model<InventoryDocument>,
    @InjectConnection() private readonly connection: mongoose.Connection,
    private readonly transactionsService: TransactionsService,
  ) {}

  async create(createInventoryDto: CreateInventoryDto): Promise<Inventory> {
    const session = await this.connection.startSession();
    let inventory: any = {};
    await session.withTransaction(async () => {
      inventory = await this.inventoryModel.create(createInventoryDto);
      const obj: CreateTransactionDto = {
        amount: 0,
        resource: '',
        transId: '',
        transUid: '',
        playerId: createInventoryDto.playerId,
        transType: createInventoryDto.type,
        data: createInventoryDto.data,
      };
      await this.transactionsService.create(obj, session);
    });
    await session.endSession();
    return inventory;
  }

  async findAll(): Promise<Inventory[]> {
    return this.inventoryModel.find({}).lean().exec();
  }

  async find(query: any): Promise<Inventory[]> {
    return this.inventoryModel.find(query).lean().exec();
  }

  async findOne(id: string): Promise<Inventory | null> {
    return this.inventoryModel.findOne({ _id: id }).lean().exec();
  }

  async update(
    id: string,
    updateInventoryDto: UpdateInventoryDto,
  ): Promise<Inventory | null> {
    const session = await this.connection.startSession();
    let inventory: any = {};
    await session.withTransaction(async () => {
      inventory = await this.inventoryModel.findOneAndUpdate(
        { _id: id },
        updateInventoryDto,
        { new: true },
      );
      // throw new BadRequestException('cut transaction');
      if (inventory) {
        const obj: CreateTransactionDto = {
          amount: 0,
          resource: '',
          transId: '',
          transUid: '',
          playerId: inventory.playerId,
          transType: inventory.type,
          data: inventory.data,
        };
        await this.transactionsService.create(obj, session);
      }
    });
    await session.endSession();
    return inventory;
  }

  async remove(id: string): Promise<Inventory | null> {
    return this.inventoryModel.findByIdAndRemove({ _id: id }).exec();
  }
}
