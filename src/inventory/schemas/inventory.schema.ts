import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import * as mongoose from 'mongoose';

export type InventoryDocument = Inventory & Document;

@Schema({ timestamps: true })
export class Inventory {
  @Prop({ type: Types.ObjectId, required: true })
  playerId: string;

  @Prop({ type: String, default: '' })
  mainCategory: string;

  @Prop({ type: String, default: '' })
  subCategory: string;

  /*@Prop({ type: Number, default: 0 })
  level: number;*/

  @Prop({ type: String, default: '', required: true })
  type: string;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: null })
  data: any;
}

export const InventorySchema = SchemaFactory.createForClass(Inventory);
