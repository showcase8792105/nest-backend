import { Test, TestingModule } from '@nestjs/testing';
import { InventoryService } from './inventory.service';
import { Model } from 'mongoose';
import { Inventory, InventoryDocument } from './schemas/inventory.schema';
import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { TransactionsService } from '../transactions/transactions.service';
import {
  Transaction,
  TransactionDocument,
} from '../transactions/schemas/transactions.schema';

describe('InventoryService', () => {
  let service: InventoryService;
  let mockInventoryModel: Model<InventoryDocument>;
  const inventory = new Inventory();
  const inventoryId = '636a3d70dfbd9b469c1d7981';

  let transactionsService: TransactionsService;
  let mockTransactionsModel: Model<TransactionDocument>;
  const transaction = new Transaction();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InventoryService,
        TransactionsService,
        {
          provide: getConnectionToken('Database'),
          useValue: {},
        },
        {
          provide: getModelToken(Inventory.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Transaction.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockTransactionsModel = module.get<Model<TransactionDocument>>(
      getModelToken(Transaction.name),
    );
    transactionsService = module.get<TransactionsService>(TransactionsService);
    mockInventoryModel = module.get<Model<InventoryDocument>>(
      getModelToken(Inventory.name),
    );
    service = module.get<InventoryService>(InventoryService);
  });

  it('(InventoryService)should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(Inventory)should be defined', () => {
    expect(inventory).toBeDefined();
  });

  it('(InventoryModel)should be defined', () => {
    expect(mockInventoryModel).toBeDefined();
  });

  it('(TransactionsService)should be defined', () => {
    expect(transactionsService).toBeDefined();
  });

  it('(TransactionsModel)should be defined', () => {
    expect(mockTransactionsModel).toBeDefined();
  });

  it('(Transaction)should be defined', () => {
    expect(transaction).toBeDefined();
  });

  it('should get one inventory', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(inventory as InventoryDocument);
    await service.findOne(inventoryId);
    expect(spy).toBeCalled();
    expect(await service.findOne(inventoryId)).toBe(inventory);
  });

  it('should find all inventory', async () => {
    const spy = jest
      .spyOn(service, 'findAll')
      .mockResolvedValue([inventory] as InventoryDocument[]);
    await service.findAll();
    expect(spy).toBeCalled();
    expect(await service.findAll()).toStrictEqual([inventory]);
  });

  it('should find inventory', async () => {
    const query = { playerId: inventoryId };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([inventory] as InventoryDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([inventory]);
  });

  it('should remove inventory', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(inventory as InventoryDocument);
    await service.remove(inventoryId);
    expect(spy).toBeCalled();
    expect(await service.remove(inventoryId)).toBe(inventory);
  });
});
