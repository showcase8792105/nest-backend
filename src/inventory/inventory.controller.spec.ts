import { Test, TestingModule } from '@nestjs/testing';
import { InventoryController } from './inventory.controller';
import { InventoryService } from './inventory.service';
import { Model } from 'mongoose';
import { RolesService } from '../roles/roles.service';
import { Role, RoleDocument } from '../roles/schemas/roles.schema';
import { Inventory, InventoryDocument } from './schemas/inventory.schema';
import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { UpdateInventoryDto } from './dto/update-inventory.dto';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { TransactionsService } from '../transactions/transactions.service';
import {
  Transaction,
  TransactionDocument,
} from '../transactions/schemas/transactions.schema';

describe('InventoryController', () => {
  let controller: InventoryController;
  let service: InventoryService;
  let mockInventoryModel: Model<InventoryDocument>;
  const inventory = new Inventory();
  const inventoryId = '636a3d70dfbd9b469c1d7981';
  let roleService: RolesService;
  let mockRoleModel: Model<RoleDocument>;

  let transactionsService: TransactionsService;
  let mockTransactionsModel: Model<TransactionDocument>;
  const transaction = new Transaction();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InventoryController],
      providers: [
        InventoryService,
        RolesService,
        TransactionsService,
        {
          provide: getConnectionToken('Database'),
          useValue: {},
        },
        {
          provide: getModelToken(Transaction.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Inventory.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockTransactionsModel = module.get<Model<TransactionDocument>>(
      getModelToken(Transaction.name),
    );
    transactionsService = module.get<TransactionsService>(TransactionsService);
    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    roleService = module.get<RolesService>(RolesService);

    mockInventoryModel = module.get<Model<InventoryDocument>>(
      getModelToken(Inventory.name),
    );
    service = module.get<InventoryService>(InventoryService);
    controller = module.get<InventoryController>(InventoryController);
  });

  it('(InventoryController) should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('(InventoryService) should be defined', () => {
    expect(service).toBeDefined();
  });
  it('(InventoryModel) should be defined', () => {
    expect(mockInventoryModel).toBeDefined();
  });
  it('(Inventory) should be defined', () => {
    expect(inventory).toBeDefined();
  });
  it('(RolesService) should be defined', () => {
    expect(roleService).toBeDefined();
  });
  it('(RoleModel) should be defined', () => {
    expect(mockRoleModel).toBeDefined();
  });
  it('(TransactionsService)should be defined', () => {
    expect(transactionsService).toBeDefined();
  });

  it('(TransactionsModel)should be defined', () => {
    expect(mockTransactionsModel).toBeDefined();
  });

  it('(Transaction)should be defined', () => {
    expect(transaction).toBeDefined();
  });

  it('should create inventory', async () => {
    const obj: CreateInventoryDto = {
      playerId: 'test-player-id',
      mainCategory: '',
      subCategory: '',
      level: 0,
      type: '',
      data: {},
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(inventory as InventoryDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(inventory);
  });

  it('should find all inventory', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([inventory] as InventoryDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([inventory]);
  });

  it('should find with filter inventory', async () => {
    const spy = jest
      .spyOn(controller, 'find')
      .mockResolvedValue([inventory] as InventoryDocument[]);
    await controller.find({});
    expect(spy).toBeCalled();
    expect(await controller.find({})).toStrictEqual([inventory]);
  });

  it('should find one inventory', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(inventory as InventoryDocument);
    await controller.findOne(inventoryId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(inventoryId)).toStrictEqual(inventory);
  });
  it('should update inventory', async () => {
    const obj: UpdateInventoryDto = {
      playerId: 'test-player-id',
      data: {},
    };
    const spy = jest
      .spyOn(controller, 'update')
      .mockResolvedValue(inventory as InventoryDocument);
    await controller.update(inventoryId, obj);
    expect(spy).toBeCalled();
    expect(await controller.update(inventoryId, obj)).toStrictEqual(inventory);
  });

  it('should remove configuration', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(inventory as InventoryDocument);
    await controller.remove(inventoryId);
    expect(spy).toBeCalled();
    expect(await controller.remove(inventoryId)).toStrictEqual(inventory);
  });
});
