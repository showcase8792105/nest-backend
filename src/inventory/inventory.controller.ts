import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { InventoryService } from './inventory.service';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UpdateInventoryDto } from './dto/update-inventory.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { PermissionGuard } from '../permissions/guards/permission.guard';
import { Permissions } from '../permissions/decorators/permission.decorator';
import { Permission } from '../permissions/enums/permission.enum';

@Controller('inventory')
export class InventoryController {
  constructor(private readonly inventoryService: InventoryService) {}

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.WRITE)
  @Post()
  create(@Body() createInventoryDto: CreateInventoryDto) {
    return this.inventoryService.create(createInventoryDto);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get()
  findAll() {
    return this.inventoryService.findAll();
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get('find')
  find(@Query() query: any) {
    return this.inventoryService.find(query);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.inventoryService.findOne(id);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.WRITE)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateInventoryDto: UpdateInventoryDto,
  ) {
    return this.inventoryService.update(id, updateInventoryDto);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.REMOVE)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.inventoryService.remove(id);
  }
}
