import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common';
import { LocalAuthGuard } from './auth/guards/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { AnonymousAuthGuard } from './auth/guards/anonymous-auth.guard';
import { BlackListGuard } from './black-list/guards/black-list.guard';
import { RefreshAuthDto } from './auth/dto/refresh-auth.dto';
import { LogoutAuthDtoDto } from './auth/dto/logout-auth.dto';

@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @UseGuards(BlackListGuard)
  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req: any) {
    return this.authService.login(req.user);
  }

  @UseGuards(BlackListGuard)
  @UseGuards(AnonymousAuthGuard)
  @Post('auth/anonymous')
  async anonymous(@Request() req: any) {
    return this.authService.anonymous(req.user);
  }

  @UseGuards(BlackListGuard)
  @Post('auth/refresh')
  async refresh(@Request() req: any, @Body() refreshAuthDto: RefreshAuthDto) {
    return this.authService.refresh(refreshAuthDto);
  }

  @Post('auth/logout')
  async logout(
    @Request() req: any,
    @Body() logoutAuthDtoDto: LogoutAuthDtoDto,
  ) {
    return this.authService.logout(logoutAuthDtoDto);
  }
}
