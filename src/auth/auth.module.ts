import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { LocalStrategy } from './strategy/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './strategy/jwt.strategy';
import { AnonymousStrategy } from './strategy/anonymous.strategy';

@Module({
  providers: [AuthService, JwtStrategy, AnonymousStrategy, LocalStrategy],
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.defaultToken.secret,
      signOptions: jwtConstants.defaultToken.signOptions,
    }),
  ],
  exports: [AuthService],
})
export class AuthModule {}
