import { IsNotEmpty } from 'class-validator';

// response for refresh route
export class RefreshResultAuthDto {
  @IsNotEmpty()
  access_token: string;
}
