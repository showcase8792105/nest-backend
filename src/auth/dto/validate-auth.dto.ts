// user validate object
export class ValidateAuthDto {
  uid?: string;
  email?: string;
  password?: string;
}
