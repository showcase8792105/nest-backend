// user validate object
import { IsNotEmpty } from 'class-validator';

export class JwtPayloadAuthDto {
  @IsNotEmpty()
  username: string;
  @IsNotEmpty()
  role: string;
  @IsNotEmpty()
  type: string;
  @IsNotEmpty()
  sub: string;
}
