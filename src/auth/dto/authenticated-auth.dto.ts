import { User } from '../../users/schemas/users.schema';

/**
 * response of the successful authentication
 */
export class AuthenticatedAuthDto {
  access_token: string;
  refresh_token?: string;
  user: User;
}
