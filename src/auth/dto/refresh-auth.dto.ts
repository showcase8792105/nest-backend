import { IsNotEmpty } from 'class-validator';

// request body for refresh route
export class RefreshAuthDto {
  @IsNotEmpty()
  userId: string;
  @IsNotEmpty()
  refresh_token: string;
}
