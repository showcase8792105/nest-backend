import { IsNotEmpty } from 'class-validator';

// request body for logout route
export class LogoutAuthDtoDto {
  @IsNotEmpty()
  userId: string;
}
