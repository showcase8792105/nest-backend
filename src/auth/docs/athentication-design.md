Anonymous User Authentication Design
=======
* Author: Stewart Sabuka
* Date: 27 Oct 2022
* version: [v1.0.0]

Problem
---------------
A new user needs to be authenticated anonymously and non-anonymously(registered), assigned a specific role and get permission to access specific resources of the application. The main target for this
is the `player`. The system should also support non-anonymous users such as `liveOps, coommunity, admin` etc which may require login with email and password

Solution
---------------
### What
* Create `auth` module which will authenticate both Anonymous and Non-anonymous (registered) user.
* route `auth/login` will authenticate Non-anonymous user using email and password
* Route `auth/anonymous` will authenticate anonymous user using uid(device id)
  - both routes will return `access_tokeen` and `user` data on success
  - route `auth/anonymous` will create new user if non exists
  
  
### Schemas
User collection will be used mostly for authentication purposes and profile collection for actual other user data

```typescript
const User = new Schema({
    type: String, // anonymous | registered
    role: String, // player | admin | gameServer | community | tester | guest
    uid: String, // anonymous user ONLY
    email: String, // non-anonymous ONLY
    password: String // non-anonymous ONLY
})

const Profile = new Schema({
 // profile fields 
})

const SuccessAuth = new Schema({
  user: User,
  access_tokeen: String
})


const AuthReq = new Schema({
  uid: String, // anonymous ONLY
  email: String, // non-anonymous ONLY
  password: String // non-anonymous ONLY
})
```

### Request
* Client will add the issued token to the header and use it in every request
* Server will use authorization guard to check/validate user request

```typescript
@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

@Post('auth/login')
async login(@Request() req: any) {
  return this.authService.login(req.user);
}

@Post('auth/anonymous')
async anonymous(@Request() req: any) {
  return this.authService.anonymous(req.user);
}
}
```

### comments


Template references
===============

### ref
[web site]

[web site]: https://docs.nestjs.com/security/authentication
