import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { ValidateAuthDto } from '../dto/validate-auth.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(LocalStrategy.name);
  constructor(private authService: AuthService) {
    super({ usernameField: 'email' });
  }

  async validate(email: string, password: string): Promise<any> {
    const obj: ValidateAuthDto = {
      email: email,
      password: password,
    };
    const user = await this.authService.validateUser(obj);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
