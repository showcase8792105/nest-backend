import { Strategy } from 'passport-custom';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { ValidateAuthDto } from '../dto/validate-auth.dto';

@Injectable()
export class AnonymousStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(AnonymousStrategy.name);
  constructor(private authService: AuthService) {
    super();
  }

  async validate(payload: any): Promise<any> {
    try {
      const { uid } = payload.body;
      const obj: ValidateAuthDto = {
        uid: uid,
      };
      return await this.authService.validateUser(obj);
    } catch (error) {
      throw new UnauthorizedException(error);
    }
  }
}
