export const jwtConstants = {
  refreshToken: {
    secret: 'ng-refresh-key',
    signOptions: { expiresIn: '90d' },
  },
  accessToken: {
    secret: 'ng-access-key',
    signOptions: { expiresIn: '1d' },
  },
  defaultToken: {
    secret: 'ng-default-key',
    signOptions: { expiresIn: '1d' },
  },
};
