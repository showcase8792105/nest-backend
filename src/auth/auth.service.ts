import {
  BadRequestException,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { ValidateAuthDto } from './dto/validate-auth.dto';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { JwtService } from '@nestjs/jwt';
import { AuthenticatedAuthDto } from './dto/authenticated-auth.dto';
import { jwtConstants } from './constants';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { RefreshAuthDto } from './dto/refresh-auth.dto';
import { RefreshResultAuthDto } from './dto/refresh-result-auth.dto';
import { JwtPayloadAuthDto } from './dto/jwt-payload-auth.dto';
import { LogoutAuthDtoDto } from './dto/logout-auth.dto';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(
    validateAuthDto: ValidateAuthDto,
  ): Promise<CreateUserDto | null> {
    if (
      validateAuthDto.uid === undefined &&
      validateAuthDto.email === undefined
    ) {
      throw new BadRequestException('you must provide unique id or email');
    }
    if (validateAuthDto.uid) {
      let user;
      user = await this.usersService.findByUid(validateAuthDto.uid);
      if (!user) {
        const obj: CreateUserDto = {
          uid: validateAuthDto.uid,
        };
        user = await this.usersService.create(obj);
      }
      const { refreshToken, ...result } = user;
      return result;
    } else if (
      validateAuthDto.email &&
      validateAuthDto.password === undefined
    ) {
      throw new BadRequestException('password is required');
    }
    if (validateAuthDto.email && validateAuthDto.password) {
      const user = await this.usersService.findByEmail(validateAuthDto.email);
      if (!user) {
        throw new UnauthorizedException('user not found');
      }
      const isMatch = await bcrypt.compare(
        validateAuthDto.password,
        user.password,
      );
      if (!isMatch) {
        throw new UnauthorizedException('invalid username or password');
      }
      const { password, refreshToken, ...result } = user;
      return result;
    }
    return null;
  }

  /**
   * genRefreshToken - generates new refresh token
   * @param payload - user payload
   */
  async genRefreshToken(payload: JwtPayloadAuthDto): Promise<string> {
    const refreshToken = this.jwtService.sign(payload, {
      secret: jwtConstants.refreshToken.secret,
      expiresIn: jwtConstants.refreshToken.signOptions.expiresIn,
    });
    const saltOrRounds = 10;
    const updateUserDto: UpdateUserDto = {};
    updateUserDto.refreshToken = await bcrypt.hash(refreshToken, saltOrRounds);
    await this.usersService.update(payload.sub, updateUserDto);
    return refreshToken;
  }

  /**
   * login - login for registered users
   * @param user
   * @param onRefresh
   */
  async login(user: any, onRefresh = false): Promise<AuthenticatedAuthDto> {
    const payload: JwtPayloadAuthDto = {
      username: user.email,
      role: user.role,
      type: user.type,
      sub: user._id,
    };
    const result: AuthenticatedAuthDto = {
      access_token: this.jwtService.sign(payload, {
        secret: jwtConstants.accessToken.secret,
        expiresIn: jwtConstants.accessToken.signOptions.expiresIn,
      }),
      user: user,
    };
    if (!onRefresh) {
      result.refresh_token = await this.genRefreshToken(payload);
    }
    return result;
  }

  /**
   * anonymous - login for anonymous user
   * @param user {User}
   * @param onRefresh {boolean} -checks if the request is refreshing token or authenticate
   */
  async anonymous(user: any, onRefresh = false): Promise<AuthenticatedAuthDto> {
    const payload: JwtPayloadAuthDto = {
      username: user.uid,
      role: user.role,
      type: user.type,
      sub: user._id,
    };
    const result: AuthenticatedAuthDto = {
      access_token: this.jwtService.sign(payload, {
        secret: jwtConstants.accessToken.secret,
        expiresIn: jwtConstants.accessToken.signOptions.expiresIn,
      }),
      user: user,
    };
    if (!onRefresh) {
      result.refresh_token = await this.genRefreshToken(payload);
    }
    return result;
  }

  /**
   * refresh - refresh expired token
   * @param refreshAuthDto {RefreshAuthDto}
   */
  async refresh(refreshAuthDto: RefreshAuthDto): Promise<RefreshResultAuthDto> {
    const user = await this.usersService.findOne(refreshAuthDto.userId);
    if (!user) {
      throw new UnauthorizedException('user not found');
    }
    const isMatch = await bcrypt.compare(
      refreshAuthDto.refresh_token,
      user.refreshToken,
    );
    if (!isMatch) {
      throw new UnauthorizedException('invalid refresh token');
    }
    const { password, refreshToken, ...result } = user; // remove sensitive data
    if (user.type === 'registered') {
      return await this.login(result, true);
    } else {
      return await this.anonymous(result, true);
    }
  }

  async logout(logoutAuthDtoDto: LogoutAuthDtoDto): Promise<any> {
    const updateUser: UpdateUserDto = {
      refreshToken: '',
    };
    await this.usersService.update(logoutAuthDtoDto.userId, updateUser); // remove refresh token from user
    return { message: 'successfully logged out' };
  }
}
