import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { Model } from 'mongoose';
import { User, UserDocument } from '../users/schemas/users.schema';
import { getModelToken } from '@nestjs/mongoose';
import { ValidateAuthDto } from './dto/validate-auth.dto';
import { AuthenticatedAuthDto } from './dto/authenticated-auth.dto';

describe('AuthService', () => {
  let authService: AuthService;
  let jwtService: JwtService;
  let usersService: UsersService;
  let mockUserModel: Model<UserDocument>;
  const user = new User();
  const authenticated = new AuthenticatedAuthDto();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(User.name),
          useValue: Model,
        },
        UsersService,
        AuthService,
        JwtService,
      ],
    }).compile();

    mockUserModel = module.get<Model<UserDocument>>(getModelToken(User.name));
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
    authService = module.get<AuthService>(AuthService);
  });

  it('(AuthService) should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('(UsersService) should be defined', () => {
    expect(usersService).toBeDefined();
  });

  it('(JwtService) should be defined', () => {
    expect(jwtService).toBeDefined();
  });

  it('should validate user', async () => {
    const obj: ValidateAuthDto = {
      uid: '23f.kuj.99uy.1aq',
    };
    const spy = jest
      .spyOn(authService, 'validateUser')
      .mockResolvedValue(user as UserDocument);
    await authService.validateUser(obj);
    expect(spy).toBeCalled();
    expect(await authService.validateUser(obj)).toStrictEqual(user);
  });

  it('should login with email and password', async () => {
    const obj: ValidateAuthDto = {
      email: 'test@gmail.com',
      password: '12345',
    };
    const spy = jest
      .spyOn(authService, 'login')
      .mockResolvedValue(authenticated);
    await authService.login(obj);
    expect(spy).toBeCalled();
    expect(await authService.login(obj)).toStrictEqual(authenticated);
  });

  it('should login anonymously', async () => {
    const obj: ValidateAuthDto = {
      uid: '23f.kuj.99uy.1aq',
    };

    const spy = jest
      .spyOn(authService, 'anonymous')
      .mockResolvedValue(authenticated);
    await authService.anonymous(obj);
    expect(spy).toBeCalled();
    expect(await authService.anonymous(obj)).toStrictEqual(authenticated);
  });
});
