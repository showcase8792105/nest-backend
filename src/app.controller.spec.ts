import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { User, UserDocument } from './users/schemas/users.schema';
import { getModelToken } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { UsersService } from './users/users.service';
import { ValidateAuthDto } from './auth/dto/validate-auth.dto';
import { AuthenticatedAuthDto } from './auth/dto/authenticated-auth.dto';
import { BlackListService } from './black-list/black-list.service';
import {
  BlackList,
  BlackListDocument,
} from './black-list/schemas/black-list.schema';

describe('AppController', () => {
  let appController: AppController;
  let authService: AuthService;
  let appService: AppService;
  let jwtService: JwtService;
  let blackListService: BlackListService;
  let mockBlackLisModel: Model<BlackListDocument>;
  let usersService: UsersService;
  let mockUserModel: Model<UserDocument>;
  const authenticated = new AuthenticatedAuthDto();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        AppService,
        UsersService,
        JwtService,
        BlackListService,
        AuthService,
        {
          provide: getModelToken(User.name),
          useValue: Model,
        },
        {
          provide: getModelToken(BlackList.name),
          useValue: Model,
        },
      ],
    }).compile();

    mockBlackLisModel = module.get<Model<BlackListDocument>>(
      getModelToken(BlackList.name),
    );
    mockUserModel = module.get<Model<UserDocument>>(getModelToken(User.name));
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
    authService = module.get<AuthService>(AuthService);
    blackListService = module.get<BlackListService>(BlackListService);
    appService = module.get<AppService>(AppService);
    appController = module.get<AppController>(AppController);
  });

  it('(AppController) should be defined', () => {
    expect(appController).toBeDefined();
  });

  it('(AuthService) should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('(AppService) should be defined', () => {
    expect(appService).toBeDefined();
  });

  it('should login', async () => {
    const obj: ValidateAuthDto = {
      email: 'test@gmail.com',
      password: '12345',
    };
    const spy = jest
      .spyOn(appController, 'login')
      .mockResolvedValue(authenticated);
    await appController.login(obj);
    expect(spy).toBeCalled();
    expect(await appController.login(obj)).toStrictEqual(authenticated);
  });

  it('should login anonymously', async () => {
    const obj: ValidateAuthDto = {
      uid: '23f.kuj.99uy.1aq',
    };

    const spy = jest
      .spyOn(appController, 'anonymous')
      .mockResolvedValue(authenticated);
    await appController.anonymous(obj);
    expect(spy).toBeCalled();
    expect(await appController.anonymous(obj)).toStrictEqual(authenticated);
  });
});
