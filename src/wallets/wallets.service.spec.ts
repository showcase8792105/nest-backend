import { Test, TestingModule } from '@nestjs/testing';
import { WalletsService } from './wallets.service';
import { Model } from 'mongoose';
import { Wallet, WalletDocument } from './schemas/wallets.schema';
import { getModelToken } from '@nestjs/mongoose';

describe('WalletsService', () => {
  let service: WalletsService;
  let mockWalletModel: Model<WalletDocument>;
  const wallet = new Wallet();
  const walletId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WalletsService,
        {
          provide: getModelToken(Wallet.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockWalletModel = module.get<Model<WalletDocument>>(
      getModelToken(Wallet.name),
    );
    service = module.get<WalletsService>(WalletsService);
  });

  it('(WalletsService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(WalletModel) should be defined', () => {
    expect(mockWalletModel).toBeDefined();
  });

  it('(Wallet) should be defined', () => {
    expect(wallet).toBeDefined();
  });

  it('should get one wallet', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(wallet as WalletDocument);
    await service.findOne(walletId);
    expect(spy).toBeCalled();
    expect(await service.findOne(walletId)).toBe(wallet);
  });

  it('should find all wallet', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([wallet] as WalletDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([wallet]);
  });

  it('should find wallet', async () => {
    const query = { playerId: '636a3d70dfbd9b469c1d7981' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([wallet] as WalletDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([wallet]);
  });

  it('should remove wallet', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(wallet as WalletDocument);
    await service.remove(walletId);
    expect(spy).toBeCalled();
    expect(await service.remove(walletId)).toBe(wallet);
  });
});
