export class CreateWalletDto {
  playerId: string;
  softCurrency: number;
  hardCurrency: number;
}
