import { Test, TestingModule } from '@nestjs/testing';
import { WalletsController } from './wallets.controller';
import { WalletsService } from './wallets.service';
import { Model } from 'mongoose';
import { Role, RoleDocument } from '../roles/schemas/roles.schema';
import { Wallet, WalletDocument } from './schemas/wallets.schema';
import { RolesService } from '../roles/roles.service';
import { getModelToken } from '@nestjs/mongoose';
import { CreateWalletDto } from './dto/create-wallet.dto';

describe('WalletsController', () => {
  let controller: WalletsController;
  let walletsService: WalletsService;
  let rolesService: RolesService;
  let mockWalletModel: Model<WalletDocument>;
  let mockRoleModel: Model<RoleDocument>;
  const wallet = new Wallet();
  const walletId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WalletsController],
      providers: [
        WalletsService,
        RolesService,
        {
          provide: getModelToken(Wallet.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    mockWalletModel = module.get<Model<WalletDocument>>(
      getModelToken(Wallet.name),
    );
    rolesService = module.get<RolesService>(RolesService);
    walletsService = module.get<WalletsService>(WalletsService);
    controller = module.get<WalletsController>(WalletsController);
  });

  it('(WalletsController) should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('(WalletsService) should be defined', () => {
    expect(walletsService).toBeDefined();
  });

  it('(RolesService) should be defined', () => {
    expect(rolesService).toBeDefined();
  });

  it('(mockWalletModel) should be defined', () => {
    expect(mockWalletModel).toBeDefined();
  });

  it('(mockRoleModel) should be defined', () => {
    expect(mockRoleModel).toBeDefined();
  });

  it('(Wallet) should be defined', () => {
    expect(wallet).toBeDefined();
  });

  it('should create wallet', async () => {
    const obj: CreateWalletDto = {
      playerId: '636a3d70dfbd9b469c1d7981',
      softCurrency: 10,
      hardCurrency: 10,
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(wallet as WalletDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(wallet);
  });

  it('should find all wallet', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([wallet] as WalletDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([wallet]);
  });

  it('should find one wallet', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(wallet as WalletDocument);
    await controller.findOne(walletId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(walletId)).toStrictEqual(wallet);
  });

  it('should remove wallet', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(wallet as WalletDocument);
    await controller.remove(walletId);
    expect(spy).toBeCalled();
    expect(await controller.remove(walletId)).toStrictEqual(wallet);
  });
});
