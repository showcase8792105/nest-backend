import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types, Document } from 'mongoose';

export type WalletDocument = Wallet & Document;

@Schema({ timestamps: true })
// NOTE:- one wallet per currency
export class Wallet {
  @Prop({ type: Types.ObjectId, required: true })
  playerId: string;

  @Prop({ type: String, default: '' })
  currency: string;

  @Prop({ type: Number, default: 0 })
  amount: number;
}

export const WalletSchema = SchemaFactory.createForClass(Wallet);
