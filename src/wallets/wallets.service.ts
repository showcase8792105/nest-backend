import { Injectable, Logger } from '@nestjs/common';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Wallet, WalletDocument } from './schemas/wallets.schema';
import { Model } from 'mongoose';

@Injectable()
export class WalletsService {
  private readonly logger = new Logger(WalletsService.name);
  constructor(
    @InjectModel(Wallet.name) private walletModel: Model<WalletDocument>,
  ) {}
  async create(createWalletDto: CreateWalletDto): Promise<Wallet> {
    const create = new this.walletModel(createWalletDto);
    return create.save();
  }

  async findAll(): Promise<Wallet[]> {
    return this.walletModel.find().lean().exec();
  }

  async findOne(id: string): Promise<Wallet | null> {
    return this.walletModel.findOne({ _id: id }).lean().exec();
  }

  async find(query: Record<string, any>): Promise<Wallet[]> {
    return this.walletModel.find(query).lean().exec();
  }

  async update(
    id: string,
    updateWalletDto: UpdateWalletDto,
  ): Promise<Wallet | null> {
    return this.walletModel.findOneAndUpdate({ _id: id }, updateWalletDto, {
      new: true,
    });
  }

  async remove(id: string): Promise<Wallet | null> {
    return await this.walletModel.findByIdAndRemove({ _id: id }).lean().exec();
  }
}
