export enum Permission {
  WRITE = 'WRITE',
  READ = 'READ',
  REMOVE = 'REMOVE',
}
