import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Permission } from '../enums/permission.enum';
import { PERMISSION_KEY } from '../decorators/permission.decorator';
import { RolesService } from '../../roles/roles.service';
import { Role } from '../../roles/schemas/roles.schema';

@Injectable()
export class PermissionGuard implements CanActivate {
  private readonly logger = new Logger(PermissionGuard.name);
  constructor(
    private reflector: Reflector,
    private rolesService: RolesService,
  ) {}

  /**
   * canActivate
   * @param context
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredPermissions: Permission[] = this.reflector.getAllAndOverride<
      Permission[]
    >(PERMISSION_KEY, [context.getHandler(), context.getClass()]);
    if (!requiredPermissions) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const url = this.baseUrl(request.url);
    let result = true;
    for (let p = 0; p < requiredPermissions.length; p++) {
      const items: Role[] = await this.rolesService.find({
        route: url,
        permission: requiredPermissions[p],
        roles: user.role,
      });
      if (items.length < 1) {
        result = false;
        break;
      }
    }
    return result;
  }

  baseUrl(url: string): string {
    const array = url.split('/');
    if (array.length < 1) {
      throw new NotFoundException('url not found ' + url);
    }
    if (!array[1].includes('?')) {
      return '/' + array[1];
    }
    const sub = array[1].split('?');
    return '/' + sub[0];
  }
}
