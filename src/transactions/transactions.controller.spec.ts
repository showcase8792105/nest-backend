import { Test, TestingModule } from '@nestjs/testing';
import { TransactionsController } from './transactions.controller';
import { TransactionsService } from './transactions.service';
import { Model } from 'mongoose';
import { RolesService } from '../roles/roles.service';
import { Role, RoleDocument } from '../roles/schemas/roles.schema';
/*import { BlackListService } from '../black-list/black-list.service';
import {
  BlackList,
  BlackListDocument,
} from '../black-list/schemas/black-list.schema';*/
import {
  Transaction,
  TransactionDocument,
} from './schemas/transactions.schema';
import { getModelToken } from '@nestjs/mongoose';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { CreateTransactionDto } from './dto/create-transaction.dto';

describe('TransactionsController', () => {
  let controller: TransactionsController;
  let transactionsService: TransactionsService;
  let mockTransactionModel: Model<TransactionDocument>;
  let roleService: RolesService;
  let mockRoleModel: Model<RoleDocument>;
  // let blackListService: BlackListService;
  // let mockBlackListModel: Model<BlackListDocument>;
  const transaction = new Transaction();
  const transactionId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TransactionsController],
      providers: [
        TransactionsService,
        RolesService,
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Transaction.name),
          useValue: Model,
        },
        /*{
          provide: getModelToken(BlackList.name),
          useValue: Model,
        },*/
      ],
    }).compile();
    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    roleService = module.get<RolesService>(RolesService);

    /*mockBlackListModel = module.get<Model<BlackListDocument>>(
      getModelToken(BlackList.name),
    );
    blackListService = module.get<BlackListService>(BlackListService);*/

    mockTransactionModel = module.get<Model<TransactionDocument>>(
      getModelToken(Transaction.name),
    );
    transactionsService = module.get<TransactionsService>(TransactionsService);
    controller = module.get<TransactionsController>(TransactionsController);
  });

  it(' (TransactionsController) should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('(TransactionsService) should be defined', () => {
    expect(transactionsService).toBeDefined();
  });
  it('(TransactionModel) should be defined', () => {
    expect(mockTransactionModel).toBeDefined();
  });
  it('(RolesService) should be defined', () => {
    expect(roleService).toBeDefined();
  });
  it('(RoleModel) should be defined', () => {
    expect(mockRoleModel).toBeDefined();
  });
  /*it('(BlackListService) should be defined', () => {
    expect(blackListService).toBeDefined();
  });
  it('(BlackListModel) should be defined', () => {
    expect(mockBlackListModel).toBeDefined();
  });*/
  it('(Transaction) should be defined', () => {
    expect(transaction).toBeDefined();
  });
  it('should create transaction', async () => {
    const obj: CreateTransactionDto = {
      playerId: 'playerId',
      transId: '',
      transType: '',
      transUid: '',
      resource: '',
      amount: 0,
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(transaction as TransactionDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(transaction);
  });

  it('should find all transaction', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([transaction] as TransactionDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([transaction]);
  });

  it('should find one transaction', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(transaction as TransactionDocument);
    await controller.findOne(transactionId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(transactionId)).toStrictEqual(transaction);
  });
  it('should update transaction', async () => {
    const obj: UpdateTransactionDto = {
      playerId: 'id',
      transUid: '',
      resource: '',
      amount: 0,
    };
    const spy = jest
      .spyOn(controller, 'update')
      .mockResolvedValue(transaction as TransactionDocument);
    await controller.update(transactionId, obj);
    expect(spy).toBeCalled();
    expect(await controller.update(transactionId, obj)).toStrictEqual(
      transaction,
    );
  });

  it('should remove configuration', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(transaction as TransactionDocument);
    await controller.remove(transactionId);
    expect(spy).toBeCalled();
    expect(await controller.remove(transactionId)).toStrictEqual(transaction);
  });
});
