import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import mongoose from 'mongoose';

export type TransactionDocument = Transaction & Document;

@Schema()
export class Transaction {
  @Prop({ type: Types.ObjectId, required: true })
  playerId: string;

  @Prop({ type: String, default: '' })
  transId: string;

  @Prop({ type: String, default: '' })
  transType: string;

  @Prop({ type: String, default: '' })
  transUid: string;

  @Prop({ type: String, default: '' })
  resource: string;

  @Prop({ type: Number, default: 0 })
  amount: number;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: null })
  data: any;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
