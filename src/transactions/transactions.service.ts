import { Injectable } from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import {
  Transaction,
  TransactionDocument,
} from './schemas/transactions.schema';

import { nanoid } from 'nanoid/async';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectModel(Transaction.name)
    private transactionModel: Model<TransactionDocument>,
  ) {}
  async create(
    createTransactionDto: CreateTransactionDto,
    session: mongoose.ClientSession | null = null,
  ): Promise<Transaction> {
    const opts = { session };
    createTransactionDto.transUid = await nanoid(10);
    const created = new this.transactionModel(createTransactionDto);
    return created.save(opts);
  }

  async findAll(): Promise<Transaction[]> {
    return this.transactionModel.find().lean().exec();
  }

  async findOne(id: string): Promise<Transaction | null> {
    return this.transactionModel.findOne({ _id: id }).lean().exec();
  }

  async find(query: any): Promise<Transaction[]> {
    return this.transactionModel.find(query).lean().exec();
  }

  async update(
    id: string,
    updateConfigurationDto: UpdateTransactionDto,
  ): Promise<Transaction | null> {
    return this.transactionModel.findOneAndUpdate(
      { _id: id },
      updateConfigurationDto,
      { new: true },
    );
  }

  async remove(id: string): Promise<Transaction | null> {
    return this.transactionModel.findByIdAndRemove({ _id: id }).exec();
  }
}
