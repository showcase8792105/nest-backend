import { IsNotEmpty } from 'class-validator';

export class CreateTransactionDto {
  @IsNotEmpty()
  playerId: string;
  transId: string;
  transType: string;
  transUid: string;
  resource: string;
  amount: number;
  data?: any;
}
