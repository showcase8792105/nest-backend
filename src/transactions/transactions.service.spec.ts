import { Test, TestingModule } from '@nestjs/testing';
import { TransactionsService } from './transactions.service';
import { Model } from 'mongoose';
import {
  Transaction,
  TransactionDocument,
} from './schemas/transactions.schema';
import { getModelToken } from '@nestjs/mongoose';

describe('TransactionsService', () => {
  let service: TransactionsService;
  let mockTransactionModel: Model<TransactionDocument>;
  const transaction = new Transaction();
  const transactionId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransactionsService,
        {
          provide: getModelToken(Transaction.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockTransactionModel = module.get<Model<TransactionDocument>>(
      getModelToken(Transaction.name),
    );
    service = module.get<TransactionsService>(TransactionsService);
  });

  it('(TransactionsService) should be defined', () => {
    expect(service).toBeDefined();
  });
  it('(Transaction) should be defined', () => {
    expect(transaction).toBeDefined();
  });
  it('(TransactionModel) should be defined', () => {
    expect(mockTransactionModel).toBeDefined();
  });
  it('should get one transaction', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(transaction as TransactionDocument);
    await service.findOne(transactionId);
    expect(spy).toBeCalled();
    expect(await service.findOne(transactionId)).toBe(transaction);
  });

  it('should find all transaction', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([transaction] as TransactionDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([transaction]);
  });

  it('should find transaction', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([transaction] as TransactionDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([transaction]);
  });

  it('should remove transaction', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(transaction as TransactionDocument);
    await service.remove(transactionId);
    expect(spy).toBeCalled();
    expect(await service.remove(transactionId)).toBe(transaction);
  });
});
