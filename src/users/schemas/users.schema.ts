import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  type: string;

  @Prop({ required: true })
  role: string;

  @Prop({ required: false })
  uid: string; // anonymous user ONLY

  @Prop({ required: false })
  email: string; // registered user ONLY

  @Prop({ required: false })
  password: string; // registered user ONLY

  @Prop({ required: false })
  refreshToken: string; // all users
}

export const UserSchema = SchemaFactory.createForClass(User);
