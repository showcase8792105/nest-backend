import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { Model } from 'mongoose';
import { User, UserDocument } from './schemas/users.schema';
import { getModelToken } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';

describe('UsersService', () => {
  let service: UsersService;
  let mockUserModel: Model<UserDocument>;
  const user = new User();
  const userId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getModelToken(User.name),
          useValue: Model,
        },
      ],
    }).compile();

    mockUserModel = module.get<Model<UserDocument>>(getModelToken(User.name));
    service = module.get<UsersService>(UsersService);
  });

  it('(UsersService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(UsersModel) should be defined', () => {
    expect(mockUserModel).toBeDefined();
  });

  it('should find all users', async () => {
    const spy = jest
      .spyOn(service, 'findAll')
      .mockResolvedValue([user] as UserDocument[]);
    await service.findAll();
    expect(spy).toBeCalled();
    expect(await service.findAll()).toStrictEqual([user]);
  });

  it('should find one user', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(user as UserDocument);
    await service.findOne(userId);
    expect(spy).toBeCalled();
    expect(await service.findOne(userId)).toStrictEqual(user);
  });

  it('should create user', async () => {
    const obj: CreateUserDto = {
      type: 'anonymous',
      role: 'player',
      uid: '2we.98.dd4r.po',
    };
    const spy = jest
      .spyOn(service, 'create')
      .mockResolvedValue(user as UserDocument);
    await service.create(obj);
    expect(spy).toBeCalled();
    expect(await service.create(obj)).toStrictEqual(user);
  });

  it('should remove user', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(user as UserDocument);
    await service.remove(userId);
    expect(spy).toBeCalled();
    expect(await service.remove(userId)).toStrictEqual(user);
  });
});
