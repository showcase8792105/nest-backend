import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/users.schema';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}
  async create(createUserDto: CreateUserDto): Promise<User> {
    if (createUserDto.uid === undefined && createUserDto.email === undefined) {
      throw new BadRequestException('you must provide unique id or email');
    }
    if (createUserDto.uid) {
      createUserDto.type = 'anonymous';
      createUserDto.role = 'player';
      const user = await this.userModel.findOne({ uid: createUserDto.uid });
      if (user) {
        throw new BadRequestException(
          'user ' + createUserDto.uid + ' already exists',
        );
      }
    } else {
      if (createUserDto.email && createUserDto.password === undefined) {
        throw new BadRequestException('password is required');
      }
      if (createUserDto.email && createUserDto.password) {
        createUserDto.type = 'registered';
        createUserDto.role = 'live-ops';
        const saltOrRounds = 10;
        createUserDto.password = await bcrypt.hash(
          createUserDto.password,
          saltOrRounds,
        );
        const user = await this.userModel.findOne({
          email: createUserDto.email,
        });
        if (user) {
          throw new BadRequestException(
            'user ' + createUserDto.email + ' already exists',
          );
        }
      }
    }
    const createdUser = new this.userModel(createUserDto);
    return (await createdUser.save()).toObject();
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find().lean().exec();
  }

  async findOne(id: string): Promise<User | null> {
    return this.userModel.findOne({ _id: id }).lean().exec();
  }

  async findByEmail(email: string): Promise<User | null> {
    return this.userModel.findOne({ email: email }).lean().exec();
  }

  async findByUid(uid: string): Promise<User | null> {
    return this.userModel.findOne({ uid: uid }).lean().exec();
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User | null> {
    return this.userModel.findOneAndUpdate({ _id: id }, updateUserDto, {
      new: true,
    });
  }

  async remove(id: string): Promise<User | null> {
    return await this.userModel.findByIdAndRemove({ _id: id }).lean().exec();
  }
}
