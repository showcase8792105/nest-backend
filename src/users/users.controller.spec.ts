import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { RolesService } from '../roles/roles.service';
import { Model } from 'mongoose';
import { User, UserDocument } from './schemas/users.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Role, RoleDocument } from '../roles/schemas/roles.schema';
import { CreateUserDto } from './dto/create-user.dto';

describe('UsersController', () => {
  let controller: UsersController;
  let userService: UsersService;
  let roleService: RolesService;
  let mockUserModel: Model<UserDocument>;
  let mockRoleModel: Model<RoleDocument>;
  const user = new User();
  const userId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        RolesService,
        {
          provide: getModelToken(User.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
      ],
    }).compile();

    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    mockUserModel = module.get<Model<UserDocument>>(getModelToken(User.name));
    userService = module.get<UsersService>(UsersService);
    roleService = module.get<RolesService>(RolesService);
    controller = module.get<UsersController>(UsersController);
  });

  it('(UsersController) should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('(UserService) should be defined', () => {
    expect(userService).toBeDefined();
  });

  it('(RoleService) should be defined', () => {
    expect(roleService).toBeDefined();
  });

  it('(UserModel) should be defined', () => {
    expect(mockUserModel).toBeDefined();
  });

  it('(mockRoleModel) should be defined', () => {
    expect(mockRoleModel).toBeDefined();
  });

  it('should create user', async () => {
    const obj: CreateUserDto = {
      type: 'anonymous',
      role: 'player',
      uid: '2we.98.dd4r.po',
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(user as UserDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(user);
  });

  it('should find all user', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([user] as UserDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([user]);
  });

  it('should find one user', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(user as UserDocument);
    await controller.findOne(userId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(userId)).toStrictEqual(user);
  });

  it('should remove user', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(user as UserDocument);
    await controller.remove(userId);
    expect(spy).toBeCalled();
    expect(await controller.remove(userId)).toStrictEqual(user);
  });
});
