export class CreateUserDto {
  _id?: string;
  type?: string;
  role?: string;
  uid?: string;
  email?: string;
  password?: string;
  refreshToken?: string;
}
