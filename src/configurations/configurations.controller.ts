import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ConfigurationsService } from './configurations.service';
import { CreateConfigurationDto } from './dto/create-configuration.dto';
import { UpdateConfigurationDto } from './dto/update-configuration.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { PermissionGuard } from '../permissions/guards/permission.guard';
import { Permissions } from '../permissions/decorators/permission.decorator';
import { Permission } from '../permissions/enums/permission.enum';

@Controller('configurations')
export class ConfigurationsController {
  constructor(private readonly configurationsService: ConfigurationsService) {}

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.WRITE)
  @Post()
  create(@Body() createConfigurationDto: CreateConfigurationDto) {
    return this.configurationsService.create(createConfigurationDto);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get()
  findAll() {
    return this.configurationsService.findAll();
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.READ)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.configurationsService.findOne(id);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.WRITE)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateConfigurationDto: UpdateConfigurationDto,
  ) {
    return this.configurationsService.update(id, updateConfigurationDto);
  }

  @UseGuards(JwtAuthGuard, PermissionGuard)
  @Permissions(Permission.REMOVE)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.configurationsService.remove(id);
  }
}
