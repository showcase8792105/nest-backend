import { Test, TestingModule } from '@nestjs/testing';
import { ConfigurationsService } from './configurations.service';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import {
  Configuration,
  ConfigurationDocument,
} from './schemas/configurations.schema';

describe('ConfigurationsService', () => {
  let mockConfigurationModel: Model<ConfigurationDocument>;
  let service: ConfigurationsService;
  const configuration = new Configuration();
  const configurationId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConfigurationsService,
        {
          provide: getModelToken(Configuration.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockConfigurationModel = module.get<Model<ConfigurationDocument>>(
      getModelToken(Configuration.name),
    );
    service = module.get<ConfigurationsService>(ConfigurationsService);
  });

  it('(ConfigurationsService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(ConfigurationModel) should be defined', () => {
    expect(mockConfigurationModel).toBeDefined();
  });

  it('(Configuration) should be defined', () => {
    expect(configuration).toBeDefined();
  });

  it('should get one configuration', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(configuration as ConfigurationDocument);
    await service.findOne(configurationId);
    expect(spy).toBeCalled();
    expect(await service.findOne(configurationId)).toBe(configuration);
  });

  it('should find all configuration', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([configuration] as ConfigurationDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([configuration]);
  });

  it('should find configurations', async () => {
    const query = { route: '/users' };
    const spy = jest
      .spyOn(service, 'find')
      .mockResolvedValue([configuration] as ConfigurationDocument[]);
    await service.find(query);
    expect(spy).toBeCalled();
    expect(await service.find(query)).toStrictEqual([configuration]);
  });

  it('should remove configuration', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(configuration as ConfigurationDocument);
    await service.remove(configurationId);
    expect(spy).toBeCalled();
    expect(await service.remove(configurationId)).toBe(configuration);
  });
});
