import { Test, TestingModule } from '@nestjs/testing';
import { ConfigurationsController } from './configurations.controller';
import { ConfigurationsService } from './configurations.service';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  Configuration,
  ConfigurationDocument,
} from './schemas/configurations.schema';
import { CreateConfigurationDto } from './dto/create-configuration.dto';
import { UpdateConfigurationDto } from './dto/update-configuration.dto';
import { RolesService } from '../roles/roles.service';
import { Role, RoleDocument } from '../roles/schemas/roles.schema';

describe('ConfigurationsController', () => {
  let controller: ConfigurationsController;
  let configurationsService: ConfigurationsService;
  let mockConfigModel: Model<ConfigurationDocument>;
  let roleService: RolesService;
  let mockRoleModel: Model<RoleDocument>;
  const configuration = new Configuration();
  const configurationId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConfigurationsController],
      providers: [
        ConfigurationsService,
        RolesService,
        {
          provide: getModelToken(Role.name),
          useValue: Model,
        },
        {
          provide: getModelToken(Configuration.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockRoleModel = module.get<Model<RoleDocument>>(getModelToken(Role.name));
    roleService = module.get<RolesService>(RolesService);

    mockConfigModel = module.get<Model<ConfigurationDocument>>(
      getModelToken(Configuration.name),
    );
    configurationsService = module.get<ConfigurationsService>(
      ConfigurationsService,
    );
    controller = module.get<ConfigurationsController>(ConfigurationsController);
  });

  it('() should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('ConfigurationsService) should be defined', () => {
    expect(configurationsService).toBeDefined();
  });

  it('(MockConfigModel) should be defined', () => {
    expect(mockConfigModel).toBeDefined();
  });

  it('(Configuration) should be defined', () => {
    expect(configuration).toBeDefined();
  });

  it('should create configuration', async () => {
    const obj: CreateConfigurationDto = {
      key: 'season-1',
      value: {},
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(configuration as ConfigurationDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(configuration);
  });

  it('should find all configurations', async () => {
    const spy = jest
      .spyOn(controller, 'findAll')
      .mockResolvedValue([configuration] as ConfigurationDocument[]);
    await controller.findAll();
    expect(spy).toBeCalled();
    expect(await controller.findAll()).toStrictEqual([configuration]);
  });

  it('should find one configuration', async () => {
    const spy = jest
      .spyOn(controller, 'findOne')
      .mockResolvedValue(configuration as ConfigurationDocument);
    await controller.findOne(configurationId);
    expect(spy).toBeCalled();
    expect(await controller.findOne(configurationId)).toStrictEqual(
      configuration,
    );
  });
  it('should update configuration', async () => {
    const obj: UpdateConfigurationDto = {
      key: 'season-1',
      value: {},
    };
    const spy = jest
      .spyOn(controller, 'update')
      .mockResolvedValue(configuration as ConfigurationDocument);
    await controller.update(configurationId, obj);
    expect(spy).toBeCalled();
    expect(await controller.update(configurationId, obj)).toStrictEqual(
      configuration,
    );
  });

  it('should remove configuration', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(configuration as ConfigurationDocument);
    await controller.remove(configurationId);
    expect(spy).toBeCalled();
    expect(await controller.remove(configurationId)).toStrictEqual(
      configuration,
    );
  });
});
