import { Injectable, Logger } from '@nestjs/common';
import { CreateConfigurationDto } from './dto/create-configuration.dto';
import { UpdateConfigurationDto } from './dto/update-configuration.dto';
import {
  Configuration,
  ConfigurationDocument,
} from './schemas/configurations.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ConfigurationsService {
  private readonly logger = new Logger(ConfigurationsService.name);
  constructor(
    @InjectModel(Configuration.name)
    private configurationModel: Model<ConfigurationDocument>,
  ) {}
  async create(
    createConfigurationDto: CreateConfigurationDto,
  ): Promise<Configuration> {
    const createdCat = new this.configurationModel(createConfigurationDto);
    return createdCat.save();
  }

  async findAll(): Promise<Configuration[]> {
    return this.configurationModel.find().lean().exec();
  }

  async find(query: any): Promise<Configuration[]> {
    return this.configurationModel.find(query).lean().exec();
  }

  async findOne(id: string): Promise<Configuration | null> {
    return this.configurationModel.findOne({ _id: id }).lean().exec();
  }

  async update(
    id: string,
    updateConfigurationDto: UpdateConfigurationDto,
  ): Promise<Configuration | null> {
    return this.configurationModel.findOneAndUpdate(
      { _id: id },
      updateConfigurationDto,
      { new: true },
    );
  }

  async remove(id: string): Promise<Configuration | null> {
    return this.configurationModel.findByIdAndRemove({ _id: id }).exec();
  }
}
