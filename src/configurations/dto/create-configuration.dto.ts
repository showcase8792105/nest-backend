export class CreateConfigurationDto {
  _id?: string;
  key: string;
  value: any;
}
