import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type ConfigurationDocument = Configuration & mongoose.Document;

@Schema()
export class Configuration {
  @Prop({ required: true, unique: true })
  key: string;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: null })
  value: any;
}

export const ConfigurationSchema = SchemaFactory.createForClass(Configuration);
