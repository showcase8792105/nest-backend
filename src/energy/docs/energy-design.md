Energy Design
=======
* Author: Stewart Sabuka
* Date: 16 December 2022
* version: [v1.0.0]

Overview
-----------
Generic Energy System  that can be used to limit/slowdown characters, weapons etc

## Collections
### Configuration
* energy type
* maximum amount
* energy refill rate
* default starting amount

### Player Energy
* player id
* energy Type
* amount
* energy refill rate
* last refill time

## Module/Service
### Energy Module
The energy module will provide two routes `status` and `consume` they will both return the current energy status

### POST /energy/status
- check current status for a specific energy
- upon request create new energy with default values if non exists
- upon request if `amount` is not maximum update `amount` and `refilledAt` based on the `refillRate`

#### Req Body
```typescript
const body: StatusReqDto = { 
  playerId: String,
  type: String,
}
```
#### Res Body
```typescript
const body: ConsumeReqDto = {
  type: String, // energy type
  amount: Number, // current amount 
  maxAmount: Number, // max energy possible 
  nextRefillAt: String, // next possible refill 
}
```

### POST /energy/consume
-  consume/deduct amount of  specific energy
- upon request create new energy with default values if non exists
- return costume error `NotEnoughEnergyException` if balance is below the amount to be consumed

#### Req Body
```typescript
const body: ConsumeReqDto = { 
  playerId: String,
  type: String,
  amount: String,
}
```
#### Res Body
```typescript
const body: ConsumeReqDto = {
  type: String,
  amount: Number,
  maxAmount: Number,
  nextRefillAt: String,
}
```
### comments
- in future consider calling `energy/status` before calling `energy/consume`
  to make sure everything is updated before consuming..

Template references
===============

### ref
[web site]

[web site]: https://docs.nestjs.com/security/authentication
