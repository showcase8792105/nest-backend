import { IsNotEmpty } from 'class-validator';
import { RefillRateDto } from './energy-config.dto';
import { Types } from 'mongoose';

export class CreateEnergyDto {
  @IsNotEmpty()
  playerId: Types.ObjectId;
  type: string;
  amount: number;
  refilledAt?: string;
  refillRate: RefillRateDto;
}
