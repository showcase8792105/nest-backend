import { IsNotEmpty } from 'class-validator';
import { Types } from 'mongoose';

export class StatusReqDto {
  @IsNotEmpty()
  playerId: Types.ObjectId;
  @IsNotEmpty()
  type: string;
}
