import { IsNotEmpty } from 'class-validator';

export class EnergyStatusDto {
  @IsNotEmpty()
  type: string;
  amount: number;
  maxAmount: number;
  nextRefillAt?: string;
}
