import { IsNotEmpty } from 'class-validator';

export class RefillRateDto {
  @IsNotEmpty()
  amount: number;
  @IsNotEmpty()
  duration: number;
  @IsNotEmpty()
  unit: string;
}

export class EnergyConfigDto {
  @IsNotEmpty()
  max: number;
  @IsNotEmpty()
  startAmount: number;
  @IsNotEmpty()
  refillRate: RefillRateDto;
}
