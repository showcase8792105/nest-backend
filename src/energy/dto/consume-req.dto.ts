import { IsNotEmpty } from 'class-validator';
import { Types } from 'mongoose';

export class ConsumeReqDto {
  @IsNotEmpty()
  playerId: Types.ObjectId;
  @IsNotEmpty()
  type: string;
  @IsNotEmpty()
  amount: number;
}
