import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { EnergyService } from './energy.service';
import { CreateEnergyDto } from './dto/create-energy.dto';
import { UpdateEnergyDto } from './dto/update-energy.dto';
import { StatusReqDto } from './dto/status-req.dto';
import { ConsumeReqDto } from './dto/consume-req.dto';

@Controller('energy')
export class EnergyController {
  constructor(private readonly energyService: EnergyService) {}

  @Post()
  create(@Body() createEnergyDto: CreateEnergyDto) {
    return this.energyService.create(createEnergyDto);
  }

  @Post('status')
  status(@Body() statusReqDto: StatusReqDto) {
    return this.energyService.status(statusReqDto);
  }

  @Post('consume')
  consume(@Body() consumeReqDto: ConsumeReqDto) {
    return this.energyService.consume(consumeReqDto);
  }

  @Get()
  findAll() {
    return this.energyService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.energyService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEnergyDto: UpdateEnergyDto) {
    return this.energyService.update(id, updateEnergyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.energyService.remove(id);
  }
}
