import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { CreateEnergyDto } from './dto/create-energy.dto';
import { UpdateEnergyDto } from './dto/update-energy.dto';
import { Energy, EnergyDocument } from './schemas/energy.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { StatusReqDto } from './dto/status-req.dto';
import { EnergyStatusDto } from './dto/energy-status.dto';
import * as moment from 'moment';
import { ConfigurationsService } from '../configurations/configurations.service';
import { EnergyConfigDto } from './dto/energy-config.dto';
import { ConsumeReqDto } from './dto/consume-req.dto';
import {
  InvalidTimeUnitException,
  NotEnoughEnergyException,
} from '../common/exceptions';

@Injectable()
export class EnergyService {
  private readonly logger = new Logger(EnergyService.name);
  constructor(
    @InjectModel(Energy.name) private energyModel: Model<EnergyDocument>,
    private readonly configurationsService: ConfigurationsService,
  ) {}
  async create(createEnergyDto: CreateEnergyDto): Promise<Energy> {
    const create = new this.energyModel(createEnergyDto);
    return create.save();
  }

  async status(statusReqDto: StatusReqDto): Promise<EnergyStatusDto | null> {
    const config = await this.getConfig(statusReqDto.type);
    const energy: Energy = await this.getOrCreateEnergy(
      statusReqDto.type,
      statusReqDto.playerId,
      config,
    );
    if (energy.amount >= config.max) {
      return {
        type: energy.type,
        amount: energy.amount,
        maxAmount: config.max,
        nextRefillAt: moment()
          .add(energy.refillRate.duration, energy.refillRate.unit)
          .toISOString(),
      };
    } else {
      return await this.updateStatus(energy, config);
    }
  }

  async consume(consumeReqDto: ConsumeReqDto): Promise<EnergyStatusDto | null> {
    const config = await this.getConfig(consumeReqDto.type);
    const energy: Energy = await this.getOrCreateEnergy(
      consumeReqDto.type,
      consumeReqDto.playerId,
      config,
    );

    if (energy.amount < consumeReqDto.amount) {
      throw new NotEnoughEnergyException({
        balance: energy.amount,
        consume: consumeReqDto.amount,
      });
    }
    const update: UpdateEnergyDto = {
      amount: energy.amount - consumeReqDto.amount,
      refillRate: config.refillRate,
    };
    if (energy.refilledAt === undefined) {
      update.refilledAt = moment().toISOString();
    }
    const result = await this.update(energy._id, update);
    if (result) {
      return {
        type: result.type,
        amount: result.amount,
        maxAmount: config.max,
        nextRefillAt: result.refilledAt,
      };
    }
    return null;
  }

  async updateStatus(
    energy: Energy,
    config: EnergyConfigDto,
  ): Promise<EnergyStatusDto | null> {
    if (energy.amount >= config.max) {
      return {
        type: energy.type,
        amount: energy.amount,
        maxAmount: config.max,
        nextRefillAt: energy.refilledAt,
      };
    }
    const item = this.getDuration(energy.refilledAt, config.refillRate.unit);
    const newAmount =
      (config.refillRate.amount * item.duration) / config.refillRate.duration;
    let totalEnergy = Math.round(newAmount + energy.amount);
    if (totalEnergy >= config.max) {
      totalEnergy = config.max;
    }
    const obj: UpdateEnergyDto = {
      amount: totalEnergy,
      refillRate: config.refillRate,
      refilledAt: moment().toISOString(),
    };
    const result = await this.update(energy._id, obj);
    if (result) {
      return {
        type: result.type,
        amount: result.amount,
        maxAmount: config.max,
        nextRefillAt: moment()
          .add(result.refillRate.duration, result.refillRate.unit)
          .toISOString(),
      };
    }
    return null;
  }

  private async getConfig(type: string): Promise<EnergyConfigDto> {
    const configs = await this.configurationsService.find({
      key: type,
    });
    if (configs.length < 1) {
      throw new NotFoundException('energy type ' + type + ' not found');
    }
    return configs[0].value;
  }

  private async getOrCreateEnergy(
    type: string,
    playerId: Types.ObjectId,
    config: EnergyConfigDto,
  ): Promise<Energy> {
    let energy: Energy;
    const _energy = await this.energyModel
      .findOne({ playerId: playerId, type: type })
      .lean()
      .exec();
    if (_energy == null) {
      const createEnergyDto: CreateEnergyDto = {
        playerId: playerId,
        type: type,
        amount: config.startAmount,
        refillRate: config.refillRate,
        refilledAt: moment().toISOString(),
      };
      energy = await this.create(createEnergyDto);
    } else {
      energy = _energy;
    }
    return energy;
  }

  getDuration(date: string, timeUnit: string) {
    const current = moment();
    const difference = moment(current).diff(date);
    if (timeUnit === 'hours') {
      const unit = moment.duration(difference).asHours();
      return { duration: Math.round(unit), unit: 'hours' };
    } else if (timeUnit === 'minutes') {
      const unit = moment.duration(difference).asMinutes();
      return { duration: Math.round(unit), unit: 'minutes' };
    } else if (timeUnit === 'seconds') {
      const unit = moment.duration(difference).asSeconds();
      return { duration: Math.round(unit), unit: 'seconds' };
    } else {
      throw new InvalidTimeUnitException({
        options: 'hours, minutes, seconds',
      });
    }
  }

  async findAll(): Promise<Energy[]> {
    return this.energyModel.find().lean().exec();
  }

  async findOne(id: string): Promise<Energy | null> {
    return this.energyModel.findOne({ _id: id }).lean().exec();
  }

  async find(query: Record<string, any>): Promise<Energy[]> {
    return this.energyModel.find(query).lean().exec();
  }

  async update(
    id: string,
    updateEnergyDto: UpdateEnergyDto,
  ): Promise<Energy | null> {
    return this.energyModel.findOneAndUpdate({ _id: id }, updateEnergyDto, {
      new: true,
    });
  }

  async remove(id: string): Promise<Energy | null> {
    return await this.energyModel.findByIdAndRemove({ _id: id }).lean().exec();
  }
}
