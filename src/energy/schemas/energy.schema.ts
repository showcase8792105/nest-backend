import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import * as mongoose from 'mongoose';

export type EnergyDocument = Energy & Document;

@Schema({ timestamps: true })
export class Energy {
  @Prop({ type: Types.ObjectId })
  _id: any;

  @Prop({ type: Types.ObjectId, required: true })
  playerId: Types.ObjectId;

  @Prop({ type: String, default: '' })
  type: string;

  @Prop({ type: Number, default: '' })
  amount: number;

  @Prop({ type: Date })
  refilledAt: string;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: null })
  refillRate: any;
}

export const EnergySchema = SchemaFactory.createForClass(Energy);
