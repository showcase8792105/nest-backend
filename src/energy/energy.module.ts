import { Module } from '@nestjs/common';
import { EnergyService } from './energy.service';
import { EnergyController } from './energy.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { RolesModule } from '../roles/roles.module';
import { Energy, EnergySchema } from './schemas/energy.schema';
import { ConfigurationsModule } from '../configurations/configurations.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Energy.name, schema: EnergySchema }]),
    RolesModule,
    ConfigurationsModule,
  ],
  controllers: [EnergyController],
  providers: [EnergyService],
})
export class EnergyModule {}
