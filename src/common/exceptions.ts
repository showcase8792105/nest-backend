import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';

import { MongoError } from 'mongodb';
import { ExceptionBlackListDto } from '../black-list/dto/exception-black-list.dto';

@Catch(MongoError)
export class MongoExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(MongoExceptionFilter.name);
  catch(exception: MongoError, host: ArgumentsHost) {
    const request = host.switchToHttp().getRequest();
    const response = host.switchToHttp().getResponse();

    switch (exception.code) {
      case 11000:
        this.logger.warn(exception);
        return response.status(HttpStatus.CONFLICT).send({
          statusCode: HttpStatus.CONFLICT,
          orgCode: exception.code,
          name: exception.name,
          message: exception.message,
          timestamp: new Date().toISOString(),
          path: request.method + request.url,
        });
      default:
        this.logger.warn(exception);
        return response.status(HttpStatus.AMBIGUOUS).send({
          statusCode: HttpStatus.AMBIGUOUS,
          orgCode: exception.code,
          message: exception.message,
          timestamp: new Date().toISOString(),
          path: request.method + request.url,
        });
    }
  }
}

export class BlackListedException extends HttpException {
  constructor(blackList: ExceptionBlackListDto) {
    super(
      { status: HttpStatus.FORBIDDEN, error: 'BlackListed', data: blackList },
      HttpStatus.FORBIDDEN,
    );
  }
}

export class NotEnoughEnergyException extends HttpException {
  constructor(obj: any) {
    super(
      { status: HttpStatus.FORBIDDEN, error: 'NotEnoughEnergy', data: obj },
      HttpStatus.FORBIDDEN,
    );
  }
}

export class InvalidTimeUnitException extends HttpException {
  constructor(obj: any) {
    super(
      { status: HttpStatus.FORBIDDEN, error: 'InvalidTimeUnit', data: obj },
      HttpStatus.BAD_REQUEST,
    );
  }
}
